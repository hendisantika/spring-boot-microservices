<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE html
PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <link rel="stylesheet" href="<c:url value="resources/static/styles/bootstrap-3.3.7/css/bootstrap.css" />" />
    <link rel="stylesheet" href="<c:url value="resources/staticstyles/bootstrap-3.3.7/css/bootstrap-theme.css" />" />
    <link rel="stylesheet" href="<c:url value="resources/static/styles/pivotal.css" />" />
    <title>spring-microservices: Home</title>
</head>

<body>

<div class="container">
    <div class="row">
        <nav class="navbar navbar-inverse">
            <div class="container-fluid">
                <div class="navbar-header">
                    <a title="Spring IO" href="http://www.spring.io">
                        <img src="<c:url value="resources/static/images/spring-trans-dark.png"/>" height="50"/>
                    </a>
                </div>
                <div>
                    <ul class="nav navbar-nav navbar-right">
                        <li>
                            <a href="http://www.pivotal.io">
                                <img alt="Pivotal" title="Pivotal" height="20" src="<c:url value="resources/static/images/pivotal-logo-600.png" />" />
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    </div>

    <div class="row">

        <h1>Accounts Web - Home Page</h1>

        <ul>
            <li><a href="/accountList">View Account List</a></li>
        </ul>

    </div>

</div>

</body>
</html>