package com.hendisantika.webclientmicroserviceserver;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 8/30/17
 * Time: 6:56 AM
 * To change this template use File | Settings | File Templates.
 */
public interface AccountRepository {
    List<Account> getAllAccounts();

    Account getAccount(String number);
}
