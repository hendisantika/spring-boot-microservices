# Spring Boot Microservices

#### 1. Introduction

Microservices is not a new term. It coined in 2005 by Dr Peter Rodgers then called micro web services based on SOAP. It became more popular since 2010. Micoservices allows us to break our large system into number of independent collaborating processes. Lets see below microservices architecture. 


#### 2. What is Microservices Architecture?
Microservices architecture allows to avoid monolith application for large system. It provide loose coupling between collaborating processes which running independently in different environments with tight cohesion. So lets discuss it by an example as below.

For example imagine an online shop with separate microservices for user-accounts, product-catalog order-processing and shopping carts. So these components are inevitably important for such a large online shopping portal. For online shopping system we could use following architectures.


### Microservices provide the same strength as Spring provide

* Loose Coupling– Application build from collaboration services or processes, so any process change without effecting another processes.
* Tight Cohesion-An individual service or process that deals with a single view of data.

There are a number of moving parts that you have to setup and configure to build such a system. For implementing this system is not too obvious you have to knowledge about spring boot, spring cloud and Netflix. In this post I will discuss one example for this architecture before the example lets first discuss about pros and cons of microservices architecture.


### 2. Microservices Benefits

* Smaller code base is easy to maintain.
* Easy to scale as individual component.
* Technology diversity i.e. we can mix libraries, databases, frameworks etc.
* Fault isolation i.e. a process failure should not bring whole system down.
* Better support for smaller and parallel team.
* Independent deployment
* Deployment time reduce

### 3. Microservices Challenges

* Difficult to achieve strong consistency across services
* ACID transactions do not span multiple processes.
* Distributed System so hard to debug and trace the issues
* Greater need for end to end testing
* Required cultural changes in across teams like Dev and Ops working together even in same team.

### 4. Microservices Infrastructure

* Platform as a Service like Pivotal Cloud Foundry help to deployment, easily run, scale, monitor etc.
* It support for continuous deployment, rolling upgrades fo new versions of code, running multiple versions of same service at same time.


