package com.hendisantika.accountsmicroserviceserver;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by IntelliJ IDEA.
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 8/30/17
 * Time: 6:36 AM
 * To change this template use File | Settings | File Templates.
 */

@Repository
public class StubAccountRepository implements AccountRepository {
    private final Logger logger = LoggerFactory.getLogger(StubAccountRepository.class);
    private final Map<String, Account> accountsByNumber = new HashMap<String, Account>();

    public StubAccountRepository() {
        Account account = new Account(1000l, "Hendi", "5115");
        accountsByNumber.put("5115", account);
        account = new Account(2000l, "Sasuke", "2089");
        accountsByNumber.put("2089", account);
        account = new Account(3000l, "Kakashi", "1286");
        accountsByNumber.put("1286", account);
        account = new Account(4000l, "Sakura", "1287");
        accountsByNumber.put("1287", account);
        account = new Account(5000l, "Minato", "1288");
        accountsByNumber.put("1288", account);
        account = new Account(6000l, "Madara", "1289");
        accountsByNumber.put("1289", account);
        account = new Account(7000l, "Sarutobi", "1290");
        accountsByNumber.put("1290", account);
        account = new Account(8000l, "Hashirama", "1291");
        accountsByNumber.put("1291", account);
        account = new Account(9000l, "Tobirama", "1292");
        accountsByNumber.put("1292", account);
        account = new Account(10000l, "Gaara", "1293");
        accountsByNumber.put("1293", account);

        logger.info("Created StubAccountRepository");
    }

    @Override
    public List<Account> getAllAccounts() {
        return new ArrayList<Account>(accountsByNumber.values());
    }

    @Override
    public Account getAccount(String number) {
        return accountsByNumber.get(number);
    }
}
